# Booking-App

#### Basic Scala backend

##### Description
Create a system that models the entities and functionality of a simple hotel booking application. No need to persist data, you can store it in memory using Scala collections.

##### Acceptance Criteria
- Defined entity "Room"
    - Can be of type: single, double, apartments
    - Has a: price, review(s), availability (based on date)  
- Defined entity "Booking"
    - Has a: room(s), total price
- Defined entity "Review"
    - Has a: rating (1 to 5), comment
- Create functionality for
    - Adding a room
    - Reviewing a room
    - Booking / freeing  a room
    - Displaying of type T
- Test that
    - The functionality defined above works as expected
    
#### Akka backend

##### Description
Create an Akka actor system that will be responsible of handling requests to the booking backend. The system should provide a certain level of fault tolerance by using a configurable number of workers.

##### Acceptance Criteria
- Have master actor responsible of receiving all booking requests
- Test that
