package ro.booking.entity

import java.time.LocalDate

case class Period(startDate: LocalDate, endDate: LocalDate)

case class Booking(id: Int, roomId: Int, rooms: Int, totalPrice: Double, period: Period)