package ro.booking.entity

import ro.booking.entity.Rating.Rating

case class Review(id: Int, roomId: Int ,rating: Rating, comment: String)

case object Rating extends Enumeration {
  type Rating = Value
  val R1, R2, R3, R4, R5 = Value
}