package ro.booking.entity

case class Room(
                 id: Int,
                 roomType: RoomType,
                 price: Double,
                 reviews : List[Review] = List.empty,
                 bookings: List[Booking] = List.empty
               )

case class Hotel(rooms: List[Room])

sealed trait RoomType{
  def isSingleRoom: Boolean = this == SingleRoom
  def isDoubleRoom: Boolean = this == DoubleRoom
  def isApartment: Boolean = this == Apartment
}

case object SingleRoom extends RoomType
case object DoubleRoom extends RoomType
case object Apartment extends RoomType