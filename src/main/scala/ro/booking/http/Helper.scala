package ro.booking.http

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.util.Timeout

import scala.concurrent.duration._

object Helper extends HotelJsonProtocol {

  def toDate(dateStr: String): LocalDate = {
    val dateRormat = "dd-MM-yyyy"
    val format = DateTimeFormatter.ofPattern(dateRormat)
    val localDate = LocalDate.parse(dateStr, format)

    localDate
  }

  val simpleRoomJsonString =
    """
      |{
      |  "bookings": [
      |    {
      |      "id": 2,
      |      "period": {
      |        "endDate": "2019-07-30",
      |        "startDate": "2019-07-25"
      |      },
      |      "roomId": 2,
      |      "rooms": 1,
      |      "totalPrice": 10.0
      |    }
      |  ],
      |  "id": 2,
      |  "price": 8.0,
      |  "reviews": [
      |    {
      |      "comment": "Perfect Room",
      |      "id": 2,
      |      "rating": "R4",
      |      "roomId": 2
      |    }
      |  ],
      |  "roomType": "DoubleRoom"
      |}
    """.stripMargin
}
