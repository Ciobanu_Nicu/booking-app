package ro.booking.http

import akka.actor.{Actor, ActorLogging}
import ro.booking.entity.Room
import ro.booking.http.Service.{CreateRoom, FindAllRooms, FindRoomByRoomType, RoomCreated}

class HotelActor extends Actor with ActorLogging {

  var rooms: List[Room] = List[Room]()

  override def receive: Receive = {
    case FindAllRooms =>
      log.info("Searching for all rooms")
      sender() ! rooms.toList

    case FindRoomByRoomType(roomType) =>
      log.info(s"Searching rooms by type: $roomType")
      sender() ! rooms.filter(_.roomType.equals(roomType))

    case CreateRoom(room) =>
      log.info(s"Adding room $room")
      rooms = room::rooms
      sender() ! RoomCreated(room)
  }
}
