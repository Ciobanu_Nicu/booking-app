package ro.booking.http

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import ro.booking.entity.Rating.Rating
import ro.booking.entity._
import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat, SerializationException}

trait HotelJsonProtocol extends DefaultJsonProtocol {


  def serializationError(msg: String) = throw new SerializationException(msg)

  implicit def roomTypeJsonFormat: JsonFormat[RoomType] =
    new JsonFormat[RoomType] {
      override def read(json: JsValue): RoomType = json match {
        case JsString("SingleRoom") => SingleRoom
        case JsString("DoubleRoom") => DoubleRoom
        case JsString("Apartment") => Apartment
        case x => serializationError(s"RoomType deserialization error: $x")
      }

      override def write(obj: RoomType): JsValue = JsString(obj.toString)
    }

  implicit def localDateJsonFormat: JsonFormat[LocalDate] =
    new JsonFormat[LocalDate] {
      private val formatter                     = DateTimeFormatter.ISO_DATE
      override def write(x: LocalDate): JsValue = JsString(x.format(formatter))

      override def read(value: JsValue): LocalDate = value match {
        case JsString(x) => LocalDate.parse(x)
        case x  => serializationError(s"Localdate deserialization error: $x")
      }
    }

  implicit def ratingJsonFormat: JsonFormat[Rating] =
    new JsonFormat[Rating] {
      override def read(json: JsValue): Rating = json match {
        case JsString("R1") => Rating.R1
        case JsString("R2") => Rating.R2
        case JsString("R3") => Rating.R3
        case JsString("R4") => Rating.R4
        case JsString("R5") => Rating.R5
        case x => serializationError(s"Rating deserialization error: $x")
      }

      override def write(obj: Rating): JsValue = JsString(obj.toString)
    }

  implicit val periodJsonFormat = jsonFormat2(Period)
  implicit val bookingJsonFormat = jsonFormat5(Booking)
  implicit val reviewJsonFormat = jsonFormat4(Review)
  implicit val roomJsonFormat = jsonFormat5(Room)
  implicit val hotelJsonFormat = jsonFormat1(Hotel)
}
