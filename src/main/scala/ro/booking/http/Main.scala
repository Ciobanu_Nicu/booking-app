package ro.booking.http

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import ro.booking.entity._
import ro.booking.http.Helper.toDate
import ro.booking.http.Service._
import spray.json._

import scala.concurrent.Future
import scala.concurrent.duration._


object Main extends App with HotelJsonProtocol {

  implicit val system = ActorSystem("RoomSystem")
  implicit val materializer = ActorMaterializer()
  import system.dispatcher

  // setup
  implicit val defaultTimeout = Timeout(2 seconds)
  val roomState = system.actorOf(Props[HotelActor], "HotelActor")

  val period: Period =  Period(toDate("25-06-2019"), toDate("30-06-2019"))
  val singleRoomType: RoomType = SingleRoom
  val review: Review = Review(1, 1, Rating.R5, "Excelent")
  val booking: Booking = Booking(1, 1, 1, 10, period)
  val room: Room =  Room(1, SingleRoom, 8, List(review), List(booking))
  val roomList = List(room)

  roomList.foreach { room =>
    roomState ! CreateRoom(room)
  }


  val reguestHandler: HttpRequest => Future[HttpResponse] = {

    case HttpRequest(HttpMethods.GET, Uri.Path("/api/room"), _, _, _) =>
      val roomsFuture: Future[List[Room]] = (roomState ? FindAllRooms).mapTo[List[Room]]
      roomsFuture.map{rooms =>
        HttpResponse(
          entity = HttpEntity(
            ContentTypes.`application/json`,
            rooms.toJson.prettyPrint
          )
        )
      }

    case HttpRequest(HttpMethods.GET, uri @Uri.Path("/api/room/fortype"), _, _, _) =>
      val query = uri.query()
      val roomTypeOption = query.get("type").map(_.toJson.convertTo[RoomType])

      roomTypeOption match {
        case Some(roomType) =>
          val roomsFuture: Future[List[Room]] = (roomState ? FindRoomByRoomType(roomType)).mapTo[List[Room]]
          roomsFuture.map { rooms =>
            HttpResponse(
              entity = HttpEntity(
                ContentTypes.`application/json`,
                rooms.toJson.prettyPrint
              )
            )
          }
        case None => Future(HttpResponse(StatusCodes.BadRequest))
      }

    case HttpRequest(HttpMethods.POST, Uri.Path("/api/room"), _, entity, _) =>
      val strictEntityFuture = entity.toStrict(3 seconds)
      strictEntityFuture.flatMap{strictEntity =>
        val roomJsonString = strictEntity.data.utf8String
        val room = roomJsonString.parseJson.convertTo[Room]
        val roomCreatedFuture: Future[RoomCreated] = (roomState ? CreateRoom(room)).mapTo[RoomCreated]

        roomCreatedFuture.map { _ =>
          HttpResponse(
            StatusCodes.OK
          )
        }
      }

    case requst: HttpRequest =>
      requst.discardEntityBytes()
      Future(
        HttpResponse(
          status = StatusCodes.NotFound
        )
      )
  }

  Http().bindAndHandleAsync(reguestHandler, "localhost", 8080)

}
