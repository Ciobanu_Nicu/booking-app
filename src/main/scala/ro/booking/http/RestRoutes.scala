package ro.booking.http

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, MethodRejection, MissingQueryParamRejection, RejectionHandler}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import ro.booking.entity._
import ro.booking.http.Helper.toDate
import ro.booking.http.Service._
import spray.json._

import scala.concurrent.duration._

trait RestRoutes extends HotelJsonProtocol with SprayJsonSupport{

  implicit val system = ActorSystem("AkkaHttpRoutes")
  implicit val materializer = ActorMaterializer()
  import system.dispatcher

  // setup
  implicit val defaultTimeout = Timeout(2 seconds)
  val roomsState = system.actorOf(Props[HotelActor], "HotelManagementActor")

  val room: Room =  Room(1, SingleRoom, 8, List(Review(1, 1, Rating.R5, "Excelent")), List(Booking(1, 1, 1, 10, Period(toDate("25-06-2019"), toDate("30-06-2019")))))

  roomsState ! CreateRoom(room)

  val hotelRoutes =
    pathPrefix("api" / "room") {
      get {
        path("type" / Segment) { strRoomType =>
          val roomType = strRoomType.toJson.convertTo[RoomType]
          complete((roomsState ? FindRoomByRoomType(roomType)).mapTo[List[Room]])
        } ~
        parameter('type) { strRoomType =>
          val roomType = strRoomType.toJson.convertTo[RoomType]
          complete((roomsState ? FindRoomByRoomType(roomType)).mapTo[List[Room]])
        } ~
          pathEndOrSingleSlash {
            complete((roomsState ? FindAllRooms).mapTo[List[Room]])
          }
      } ~
      post {
        entity(as[Room]) { room =>
          complete((roomsState ? CreateRoom(room)).map(_ => StatusCodes.OK))
        }
      }
    }

}
