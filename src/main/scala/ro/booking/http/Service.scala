package ro.booking.http

import ro.booking.entity.{Room, RoomType}

object Service {

  case class CreateRoom(room: Room)
  case class RoomCreated(room: Room)
  case class FindRoomByRoomType(roomType: RoomType)
  case object FindAllRooms

}
