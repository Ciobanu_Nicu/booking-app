package ro.booking.service

import java.time.LocalDate

import ro.booking.entity.{Booking, Hotel, Review, Room}

object HotelService {

  def addRoom(hotel: Hotel, room: Room): Hotel = Hotel(room :: hotel.rooms)

  def addReview(hotel: Hotel, review: Review): Hotel = {
    val updatedRooms = hotel.rooms.map(room => if(room.id == review.roomId) room.copy(reviews = review :: room.reviews) else room)
    Hotel(updatedRooms)
  }

  def addBooking(hotel: Hotel, booking: Booking): Hotel = {
    val updatedBookings = hotel.rooms.map(room => if(room.id == booking.roomId) room.copy(bookings = booking :: room.bookings) else room)
    Hotel(updatedBookings)
  }

  def deleteBooking(bookingsList: List[Booking], booking: Booking): List[Booking] = {
    bookingsList.filter(_.id != booking.id)
  }

  def freeingBooking(hotel: Hotel, booking: Booking): Hotel = {
    val roomsWithUpdatedBookings = hotel.rooms.map(room => room.copy(bookings = deleteBooking(room.bookings, booking)))
    Hotel(roomsWithUpdatedBookings)
  }

    def displayChapestRoom(hotel: Hotel): List[Room] = {
      val roomWithMinPrice = hotel.rooms.min(Ordering.by((r:Room) => r.price))
      hotel.rooms.filter(_.price.equals(roomWithMinPrice.price))
    }

  def isLess(d1: LocalDate, d2: LocalDate, room: Room): Boolean = {
    room.bookings.forall(room => d1.isBefore(room.period.startDate) && d2.isBefore(room.period.endDate))
  }

  def isHigher(d1: LocalDate, d2: LocalDate, room: Room): Boolean = {
    room.bookings.forall(room => d1.isAfter(room.period.startDate) && d2.isAfter(room.period.endDate))
  }

  def displayAvailabiltyPeriod(startDate: LocalDate, endDate: LocalDate, hotel: Hotel): List[Room] = {
   hotel.rooms.filter(room => isHigher(startDate, endDate, room) || isLess(startDate, endDate, room))
  }

  def displayAvailabiltyPrices(minPrice: Double, maxPrice: Double, hotel: Hotel): List[Room] = {
    hotel.rooms.filter(room => minPrice < room.price && maxPrice > room.price)
  }
//TODO Nicu: treat all cases for all functions
}
