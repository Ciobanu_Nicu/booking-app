package service

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import org.scalatest.{Matchers, WordSpec}
import ro.booking.entity._
import ro.booking.service.HotelService

class HotelServiceTest extends WordSpec with Matchers{

  val minPrice1: Double = 5.0
  val maxPrice1: Double = 9.0
  val minPrice2: Double = 10.0
  val maxPrice2: Double = 15.0

  val period1: Period =  Period(toDate("25-05-2019"), toDate("30-05-2019"))
  val period2: Period =  Period(toDate("27-05-2019"), toDate("31-05-2019"))
  val period3: Period =  Period(toDate("20-05-2019"), toDate("24-05-2019"))

  val singleRoomType: RoomType = SingleRoom
  val doubleRoomType: RoomType = DoubleRoom
  val apartmentRoomType: RoomType = Apartment

  val review1: Review = Review(1, 1, Rating.R5, "Excelent")
  val review2: Review = Review(2, 2, Rating.R4, "Good")
  val review3: Review = Review(3, 2, Rating.R1, "Bad")

  val booking1: Booking = Booking(1, 1, 1, 10, period1)
  val booking2: Booking = Booking(2, 2, 1, 15, period2)
  val booking3: Booking = Booking(3, 2, 1, 15, period2)
  val booking4: Booking = Booking(4, 4, 1, 15, period2)
  val booking5: Booking = Booking(5, 4, 1, 15, period1)

  val room1: Room =  Room(1, SingleRoom, 8, List(review1), List(booking1))
  val room2: Room = Room(2, DoubleRoom, 15, List(review2), List(booking2))
  val room3: Room = Room(3, Apartment, 10, List(review2), List(booking2))
  val room4: Room = Room(4, Apartment, 10, List(review2), List(booking4, booking5))

  val roomWithNewReview: Room = Room(2, DoubleRoom, 15, List(review3, review2), List(booking2))
  val roomWithNewBooking: Room = Room(2, DoubleRoom, 15, List(review2), List(booking3, booking2))

  val hotel = Hotel(List(room1))
  val hotelWithFreeingBooking = Hotel(List(room4))
  val hotelWithTwoRooms = Hotel(List(room1, room2))
  val hotelWithTwoRoomsAndMultipleBooking = Hotel(List(room3, room4))
  val hotelService = HotelService

  def toDate(dateStr: String): LocalDate = {

    val dateRormat = "dd-MM-yyyy"
    val format = DateTimeFormatter.ofPattern(dateRormat)
    val localDate = LocalDate.parse(dateStr, format)

    localDate
  }

  "RoomManagement" should {

    "add a new room to the list of rooms" in {
      val newHotel = hotelService.addRoom(hotel, room2)
      newHotel shouldBe Hotel(List(room2, room1))
    }

    "add an new review to the room list of reviews" in {
      val newHotel = hotelService.addReview(hotelWithTwoRooms,review3)
      newHotel shouldBe Hotel(List(room1, roomWithNewReview))
    }

    "not add an new review to the room list of reviews" in {
      val newHotel = hotelService.addReview(hotel, review3)
      newHotel shouldBe Hotel(List(room1))
    }

    "add a new booking to the room list of bookings" in {
      val newHotel = hotelService.addBooking(hotelWithTwoRooms, booking3)
      newHotel shouldBe Hotel(List(room1, roomWithNewBooking))
    }

    "not add a new booking to the room list of bookings" in {
      val newHotel = hotelService.addBooking(hotel, booking3)
      newHotel shouldBe Hotel(List(room1))
    }

    "freeing a room from the room list of bookings" in {
      val newHotel = hotelService.freeingBooking(hotelWithFreeingBooking, booking5)
      newHotel shouldBe Hotel(List(room4.copy(bookings = List(booking4))))
    }

    "display the cheapest rooms from the list of rooms" in {
      hotelService.displayChapestRoom(hotelWithTwoRooms) shouldBe List(room1)
    }

    "display the available rooms from a period whose the reserved period is after the desired period" in {
      hotelService.displayAvailabiltyPeriod(toDate("20-05-2019"), toDate("23-05-2019"), hotelWithTwoRooms) shouldBe List(room1, room2)
    }

    "display the available rooms from a period whose the reserved period is before the desired period" in {
      hotelService.displayAvailabiltyPeriod(toDate("05-06-2019"), toDate("09-06-2019"), hotelWithTwoRoomsAndMultipleBooking) shouldBe List(room3, room4)
    }

    "display the available rooms from a certain prices" in {
      hotelService.displayAvailabiltyPrices(minPrice1, maxPrice1, hotelWithTwoRooms) shouldBe List(room1)
    }

    //TODO Nicu: treat all cases for all tests
  }
}
