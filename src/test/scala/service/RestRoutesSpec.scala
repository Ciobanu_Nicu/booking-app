package service

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.pattern.ask

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures

import ro.booking.entity._
import ro.booking.http.Helper.toDate
import ro.booking.http.RestRoutes
import ro.booking.http.Service.FindAllRooms

import scala.concurrent.ExecutionContext

class RestRoutesSpec extends FlatSpec with ScalatestRouteTest with ScalaFutures {

  trait TestRestService extends RestRoutes {
    def executionContext: ExecutionContext = executor
  }

  "RestRoutes" should "return all rooms in the hotel" in new TestRestService {
    val roomList: List[Room] = List(room)
      Get("/api/room") ~> Route.seal(hotelRoutes) ~> (check {
        assert(status == StatusCodes.OK)
        assert(entityAs[List[Room]] == roomList)
      })
    }

  it should "return a room by calling the endpoint GET /api/room/type/SingleRoom with the type in the path" in new TestRestService {
    Get("/api/room/type/SingleRoom") ~> Route.seal(hotelRoutes) ~> (check {
      assert(status == StatusCodes.OK)
      assert(entityAs[List[Room]] == List(room))
    })
  }

  it should "return a room by calling the endpoint GET /api/room?type=SingleRoom with the type in the path" in new TestRestService {
    Get("/api/room?type=SingleRoom") ~> Route.seal(hotelRoutes) ~> (check {
      assert(status == StatusCodes.OK)
      assert(entityAs[List[Room]] == List(room))
    })
  }

  it should "return a empty list by calling the endpoint GET /api/room?type=DoubleRoom with the type in the path" in new TestRestService {
    Get("/api/room?type=DoubleRoom") ~> Route.seal(hotelRoutes) ~> (check {
      assert(status == StatusCodes.OK)
      assert(entityAs[List[Room]] == List())
    })
  }

  it should "create a room by calling the endpoint POST /api/room" in new TestRestService {
    val registeredRoom: Room =  Room(2, DoubleRoom, 8, List(Review(2, 2, Rating.R4, "Good")), List(Booking(2, 2, 1, 10, Period(toDate("15-08-2019"), toDate("20-08-2019")))))
    Post("/api/room", registeredRoom) ~> Route.seal(hotelRoutes) ~> (check {
      assert(status == StatusCodes.OK)
      assert((roomsState ? FindAllRooms).mapTo[List[Room]].futureValue.contains(registeredRoom))
    })
  }
}
